import { assert, expect, should } from 'chai'
import { createApp } from "vue";
import { createStore } from "vuex";
import VuexPersistence, { MockStorage } from '../src/index'
const App = createApp({});

const mockStorage = new MockStorage()
mockStorage.setItem('vuex', JSON.stringify({
  dog: {
    barks: 2
  }
}))
const vuexPersist = new VuexPersistence<any>({
  storage: mockStorage,
  reducer: (state) => ({ dog: state.dog }),
  filter: (mutation) => (mutation.type === 'dogBark')
})

const store = createStore({
  state: {
    dog: {
      barks: 0
    },
    cat: {
      mews: 0
    }
  },
  mutations: {
    dogBark(state) {
      state.dog.barks++
    },
    catMew(state) {
      state.cat.mews++
    }
  },
  plugins: [vuexPersist.plugin]
})
App.use(store)
const getSavedStore = () => JSON.parse(mockStorage.getItem('vuex') || '')

describe('Storage: MockStorage, Test: reducer, filter, Existing Data: TRUE', () => {
  it('should persist reduced state', () => {
    store.commit('dogBark')
    expect(getSavedStore().dog.barks).to.equal(3)
  })
  it('should not persist non reduced state', () => {
    store.commit('catMew')
    expect(getSavedStore().cat).to.be.undefined
  })
})
