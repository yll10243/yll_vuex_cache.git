import { assert, expect, should } from 'chai'
import { createApp } from 'vue'
import { createStore } from 'vuex'
import VuexPersistence, { MockStorage } from '../src/index'

const App = createApp({})

const mockStorage = new MockStorage()
mockStorage.setItem('vuex', JSON.stringify({
  dog: {
    colors: ['blue']
  }
}))
const vuexPersist = new VuexPersistence<any>({
  storage: mockStorage,
  reducer: (state) => ({ dog: state.dog }),
  filter: (mutation) => (mutation.type === 'addDogColor'),
  mergeOption: "concatArrays"
})

const store = createStore({
  state: {
    dog: {
      colors: ['black', 'brown']
    },
    cat: {
      colors: ['white', 'yellow']
    }
  },
  mutations: {
    addDogColor(state) {
      state.dog.colors.push('grey')
    },
    addCatColor(state) {
      state.cat.colors.push('beige')
    }
  },
  plugins: [vuexPersist.plugin]
})
App.use(store)
const getSavedStore = () => JSON.parse(mockStorage.getItem('vuex') || "")

describe('Storage: MockStorage, Test: reducer, filter, Existing Data: TRUE', () => {
  it('should persist reduced state', () => {
    store.commit('addDogColor')
    expect(getSavedStore().dog.colors).to.deep.equal(['black', 'brown', 'blue', 'grey'])
    expect(getSavedStore().dog.colors.length).to.equal(4)
  })
  it('should not persist non reduced state', () => {
    store.commit('addCatColor')
    expect(getSavedStore().cat).to.be.undefined
  })
})
