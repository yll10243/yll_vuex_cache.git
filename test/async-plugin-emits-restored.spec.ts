import localForage from "localforage";
import { createApp } from "vue";
import { createStore } from "vuex";
import VuexPersistence from "../src/index";

const App = createApp({});
const objectStore: { [key: string]: any } = {};
const MockForageStorage = {
  _driver: "objectStorage",
  _support: true,
  _initStorage() {},
  clear() {},
  getItem<T>(key: string): Promise<T> {
    return Promise.resolve<T>(objectStore[key]);
  },
  iterate() {},
  key() {},
  keys() {},
  length() {},
  removeItem() {},
  setItem<T>(key: string, data: T): Promise<T> {
    return Promise.resolve<T>((objectStore[key] = data));
  },
};

localForage.defineDriver(MockForageStorage as any);
localForage.setDriver("objectStorage");

const vuexPersist = new VuexPersistence<any>({
  key: "restored_test",
  asyncStorage: true,
  storage: localForage,
  reducer: (state) => ({ count: state.count }),
});

const store = createStore({
  state() {
    return {
      count: 0,
    };
  },
  mutations: {
    increment(state: any) {
      state.count++;
    },
  },
  plugins: [vuexPersist.plugin],
});

describe("Storage: AsyncStorage; Test: set `restored` on store; Strict Mode: OFF", () => {
  it("connects the `store.restored` property to the Promise returned by `restoreState()`", (done) => {
    App.use(store);
    done();
  });
});
