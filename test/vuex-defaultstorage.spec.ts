import { assert, expect, should } from "chai";
import { createApp } from "vue";
import { createStore } from "vuex";
import VuexPersistence from "../src/index";

const App = createApp({});
const vuexPersist = new VuexPersistence();

const store = createStore({
  state: {
    dog: {
      barks: 0,
    },
    cat: {
      mews: 0,
    },
  },
  mutations: {
    dogBark(state) {
      state.dog.barks++;
    },
    catMew(state) {
      state.cat.mews++;
    },
  },
  plugins: [vuexPersist.plugin],
});
App.use(store);
const getSavedStore = () =>
  JSON.parse((vuexPersist.storage as Storage).getItem("vuex") as string);

describe("Storage: Default Storage, Test: reducer, filter; Strict Mode: OFF", () => {
  it("should persist reduced state", () => {
    store.commit("dogBark");
    expect(getSavedStore().dog.barks).to.equal(1);
    store.commit("catMew");
    expect(getSavedStore().cat.mews).to.equal(1);
  });
});
