import { assert, expect, should } from 'chai'
import Flatted from 'yll_jsonparser'
import { createApp } from 'vue'
import { createStore } from 'vuex'
import VuexPersistence, { MockStorage } from '../src/index'

const App = createApp({})

const mockStorage = new MockStorage()
const vuexPersist = new VuexPersistence<any>({
  supportCircular: true,
  storage: mockStorage
})

const store = createStore({
  state: {
    cyclicObject: null
  },
  mutations: {
    storeCyclicObject(state, payload) {
      state.cyclicObject = payload
    }
  },
  plugins: [vuexPersist.plugin]
})
App.use(store)
const getSavedStore = () => Flatted.parse(mockStorage.getItem('vuex') || '')

describe('Storage: MockStorage, Test: cyclic object', () => {
  it('should persist cyclic object', () => {
    const cyclicObject: any = { foo: 10 }
    cyclicObject.bar = cyclicObject
    store.commit('storeCyclicObject', cyclicObject)
    expect(getSavedStore().cyclicObject.foo).to.equal(10)
  })
})
