import { assert, expect, should } from "chai";
import { createApp } from "vue";
import { createStore } from "vuex";
import VuexPersistence, { MockStorage } from "../src/index";
const App = createApp({});
const mockStorage = new MockStorage();
mockStorage.setItem(
  "vuex",
  JSON.stringify({
    mice: {
      jerry: {
        squeeks: 2,
      },
    },
  })
);
const vuexPersist = new VuexPersistence<any>({
  strictMode: true,
  storage: mockStorage,
});

const store = createStore({
  strict: true,
  state: {
    mice: {
      jerry: {
        squeeks: 2,
      },
      mickey: {
        squeeks: 3,
      },
    },
  },
  mutations: {
    addMouse(state, name) {
      state.mice = { ...state.mice, [name]: { squeeks: 0 } };
    },
    RESTORE_MUTATION: vuexPersist.RESTORE_MUTATION,
  },
  plugins: [vuexPersist.plugin],
});

App.use(store);
describe("Storage: MockStorage; Test: observable nested objects; Existing Data: TRUE; Strict: TRUE", () => {
  it("should keep observers in nested objects", () => {
    store.commit("addMouse", "minnie");
    const tempData:any=store.state.mice
    const temp=tempData.minnie.__ob__
    if(typeof temp !=='undefined'){
      expect(temp).to.not.be.undefined;
    }
  });
});
