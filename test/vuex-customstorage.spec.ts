import { assert, expect, should } from "chai";
import { createApp } from "vue";
import { createStore } from "vuex";
import VuexPersistence from "../src/index";
const App = createApp({});

const objStorage: any = {};
const vuexPersist = new VuexPersistence<any>({
  key: "dafuq",
  restoreState: (key) => objStorage[key],
  saveState: (key, state) => {
    objStorage[key] = state;
  },
  reducer: (state) => ({ dog: state.dog }),
  filter: (mutation) => mutation.type === "dogBark",
});

const store = createStore({
  state: {
    dog: {
      barks: 0,
    },
    cat: {
      mews: 0,
    },
  },
  mutations: {
    dogBark(state) {
      state.dog.barks++;
    },
    catMew(state) {
      state.cat.mews++;
    },
  },
  plugins: [vuexPersist.plugin],
});
App.use(store);
const getSavedStore = () => objStorage["dafuq"];

describe("Storage: Custom(Object); Test: reducer, filter; Strict Mode: OFF", () => {
  it("should persist reduced state", () => {
    store.commit("dogBark");
    expect(getSavedStore().dog.barks).to.equal(1);
  });
  it("should not persist non reduced state", () => {
    store.commit("catMew");
    expect(getSavedStore().cat).to.be.undefined;
  });
});
