### 涉及到的全局的npm包

- npm install ts-node -g
- npm install nodemon -g
 
### 启动

- npm run dev终端监听ts的编译
- npm run serve通过浏览器去进行调试


### npm包的发布流程

- **npm adduser** 未注册进行注册操作
- npm login登录
- npm publish发布包上传到npm服务器
- 更新版本修改package.json文件的version的版本
- npm publish重新发布包
- 删除指定的版本npm unpublish 包名@版本号
- 删除整个包npm unpublish 包名 --force

### .fatherrc.js的配置文件

> rollup-plugin-copy的插件

- npm install rollup-plugin-copy  -D
- fatherrc.js文件中引入import copy from 'rollup-plugin-copy';
- 使用

```
...
extraRollupPlugins: [
  copy({
    targets: [{ src: 'src/index.d.ts', dest: 'dist/' }],
  }),
]
```