export default {
  cjs: 'babel', //rollup | babel
  esm: {
    type: 'babel', //rollup | babel
    importLibToEs: true,
  },
  lessInBabelMode: true,
  extraRollupPlugins: []
};
