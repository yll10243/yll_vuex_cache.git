import { MutationPayload, Plugin, Mutation } from 'vuex'

export type MergeOptionType = 'replaceArrays' | 'concatArrays'
export interface PersistOptions<S> {
  storage?: any
  restoreState?: (key: string, storage?: any) => any
  saveState?: (key: string, state: {}, storage?: any) => any
  reducer?: (state: S) => {}
  key?: string
  filter?: (mutation: MutationPayload) => boolean
  modules?: string[]
  strictMode?: boolean
  asyncStorage?: boolean
  supportCircular?: boolean
  mergeOption?: MergeOptionType
}

export interface AsyncStorage {
  _config?: {
    name: string
  }
  getItem<T>(key: string): Promise<T>
  setItem<T>(key: string, data: T): Promise<T>
  removeItem(key: string): Promise<void>
  clear(): Promise<void>
  length(): Promise<number>
  key(keyIndex: number): Promise<string>
}

export default class VuexPersistence<S> implements PersistOptions<S> {
  constructor(options?: PersistOptions<S>);

  asyncStorage: boolean
  storage: Storage | AsyncStorage | undefined
  restoreState: (key: string, storage?: AsyncStorage | Storage) => Promise<S> | S
  saveState: (key: string, state: {}, storage?: AsyncStorage | Storage) => Promise<void> | void
  reducer: (state: S) => Partial<S>
  key: string
  filter: (mutation: MutationPayload) => boolean
  modules: string[]
  strictMode: boolean
  supportCircular: boolean
  mergeOption: MergeOptionType
  plugin: Plugin<S>
  RESTORE_MUTATION: Mutation<S>
  subscribed: boolean
}
declare module 'yll_vuex_cache' { }